﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampleLibrary;
using SampleLibrary.AccessModifiers;
using SampleLibrary.OOPApproach;
using SampleLibrary.OOPApproach.Functions;
using SampleLibrary.Extensions;

namespace SampleOOP
{
    class Program
    {
      
        static void Main(string[] args)
        {
            //SampleClass sampleClass = new SampleClass();

            //Console.WriteLine("Class Name: " + sampleClass.ClassName);

        }

        private void Student() {

            HelperFunctions helper = new HelperFunctions();

            Student student = new Student();

            student.FirstName = "Christian";
            student.MiddleInitial = "I.";
            student.LastName = "Felix";
            student.Gender = "Male";
            student.Year = 1;
            student.Course = "Computer Science";
            student.StudentNo = helper.GenerateIDNumber("ST-", 10);
            student.BirthDate = ("02/06/1988").ToDateTime();

            student.GetProfile();


        }

        private void Teacher() {


        }
    }
}
