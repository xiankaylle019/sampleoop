﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleLibrary
{
    /// <summary>
    /// This class cannot be inherired
    /// </summary>
    public class SampleSealedClass
    {
        /// <summary>
        /// Sealed Class Property
        /// </summary>
        public string SealedClassProperty { get; set; }
    }
}
