﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleLibrary
{
    /// <summary>
    /// Sample class implementation
    /// </summary>
    public class SampleClass
    {
        /// <summary>
        /// Class Constructor
        /// </summary>
        public SampleClass()
        {
            //Initialiaze GetClassName method.
            this.GetClassName();
        }

        /// <summary>
        /// Class name property.
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// Private method getting class name.
        /// </summary>
        /// <returns></returns>
        private string GetClassName() {

            this.ClassName = this.GetType().Name;

            return this.ClassName;

        }
    }
}
