﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampleLibrary.OOPApproach.Interfaces;

namespace SampleLibrary.OOPApproach
{
    public sealed class Teacher : Person, IPersonDetails
    {
        public string EmployeeNo { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public float Salary { get; set; }
        public string SSSNumber { get; set; }
        public string TinNumber { get; set; }

        /// <summary>
        /// Get full name from base class implementation
        /// </summary>
        /// <returns></returns>
        public override string GetFullName()
        {
            return base.GetFullName();
        }
        /// <summary>
        /// Get Teacher Student Implementation
        /// </summary>
        public void GetProfile()
        {
            Console.WriteLine("=========Teacher Profile==========");
            Console.WriteLine("Full Name: " + GetFullName());
            Console.WriteLine("Employee No: " + this.EmployeeNo);
            Console.WriteLine("Department: " + this.Department);
            Console.WriteLine("Position: " + this.Position);

        }

    }
}
