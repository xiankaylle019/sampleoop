﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleLibrary.Extensions
{
    public static class HelperExtensions
    {
        /// <summary>
        /// Convert String date to date
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(this string date)
        {
            return DateTime.Parse(date).Date;
        }
        /// <summary>
        /// Convert string amount to float
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public static float ToFloat(this string amount) {

            return float.Parse(amount);

        }
    }
}
