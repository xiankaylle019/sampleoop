﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampleLibrary.OOPApproach.Interfaces;

namespace SampleLibrary.OOPApproach
{
    /// <summary>
    /// Derived Class Student
    /// </summary>
    public sealed class Student : Person, IPersonDetails
    {
        public string StudentNo { get; set; }
        public string Course { get; set; }
        public int Year { get; set; }
       
        /// <summary>
        /// Get full name by Student Class implementation
        /// </summary>
        /// <returns></returns>
        public override string GetFullName()
        {
            return string.Concat(this.LastName + ", ", this.FirstName);
        }

        /// <summary>
        /// Get Profile Student Implementation
        /// </summary>
        public void GetProfile() {

            Console.WriteLine("=========Student Profile==========");
            Console.WriteLine("Full Name: " + GetFullName());
            Console.WriteLine("Gender: " + this.Gender);          
            Console.WriteLine("Course: " + this.Course);
            Console.WriteLine("Year: " + this.Year);
            Console.WriteLine("Student No: " + this.StudentNo);
        }

    }
}
