﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleLibrary.OOPApproach.Interfaces
{
    public interface IPersonDetails
    {
        void GetProfile();
    }
}
