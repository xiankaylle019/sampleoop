﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampleLibrary.OOPApproach.Interfaces;

namespace SampleLibrary.OOPApproach
{
    /// <summary>
    /// Base Class Person must be inherited
    /// </summary>
    public abstract class Person : IPerson
    {
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string Gender { get; set; }
        public string Title { get; set; }

        /// <summary>
        /// Base Class Walk Method
        /// </summary>
        public virtual void Walk()
        {
            Console.WriteLine(this.Title + "." + this.LastName + " is walking.");
        }
        /// <summary>
        /// Base Class Run Method
        /// </summary>
        public virtual void Run()
        {
            Console.WriteLine(this.Title + "." + this.LastName + " is running.");
        }
        /// <summary>
        /// Base Class Talk Method
        /// </summary>
        public virtual void Talk()
        {
            Console.WriteLine(this.Title + "." + this.LastName + " is talking.");
        }
        /// <summary>
        /// Get Person full name
        /// </summary>
        /// <returns></returns>
        public virtual string GetFullName()
        {
            return string.Concat(this.Title + "." + this.FirstName + " " , this.LastName);
        }

    }
}
