﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleLibrary.OOPApproach.Functions
{
    public class HelperFunctions
    {
        /// <summary>
        /// Private property number
        /// </summary>
        private int[] Numbers
        {
            get;
            set;
        }

        /// <summary>
        /// Constructor for Helper Functions
        /// </summary>
        public HelperFunctions()
        {
            random = new Random();

            this.Numbers = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        }
        private static Random random;
        /// <summary>
        /// Generate Id Number
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public string GenerateIDNumber(string prefix, int length) {

            string id = GenerateNumber(length);

            return string.Concat(prefix, id);
        }
        /// <summary>
        /// Number generator
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        private string GenerateNumber(int length)
        {
            string id = string.Empty;

            for (int i = 1; i <= length; i++)
            {
                id += random.Next(this.Numbers[0],
                            this.Numbers[Numbers.Length - 1]);
            }

            return id;
        }
    }
}
