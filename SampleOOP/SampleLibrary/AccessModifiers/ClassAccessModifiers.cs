﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleLibrary.AccessModifiers
{
    /// <summary>
    /// This Class must be inherited 
    /// </summary>
    public class ClassAccessModifiers
    {   /// <summary>
        /// There are no restrictions on accessing public members.
        /// </summary>
        public string PublicString { get; set; }
        /// <summary>
        /// Can only access with in the class or assembly.
        /// </summary>
        private string PrivateString { get; set; }
        /// <summary>
        /// Access is limited to within the class definition and any 
        /// class that inherits from the class.
        /// </summary>
        protected string ProtectedString { get; set; }
        /// <summary>
        /// Internal Access is limited exclusively to classes defined 
        /// within the current project assembly
        /// </summary>
        internal string InternalString { get; set; }
        /// <summary>
        /// Protected Internal Access is limited to the current assembly and 
        /// types derived from the containing class. 
        /// All members in current project and all members 
        /// in derived class can access the variables.
        /// </summary>
        protected internal string ProtectedInternalString { get; set; }
    }
}
