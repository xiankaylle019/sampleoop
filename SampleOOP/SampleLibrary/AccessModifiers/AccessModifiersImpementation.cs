﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleLibrary.AccessModifiers
{
    public class AccessModifiersImpementation : ClassAccessModifiers
    {
        public AccessModifiersImpementation()
        {
            //No implementation
            
        }   
        /// <summary>
        /// Get property value from New instance of Parent Class
        /// </summary>
        public void GetProperties() {

            ClassAccessModifiers accessModifiers = new ClassAccessModifiers();

            accessModifiers.PublicString = "Public String";

            accessModifiers.InternalString = "Internal string";

            accessModifiers.ProtectedInternalString = "Protected Internal String";

            Console.WriteLine("Public String: " + accessModifiers.PublicString);
            Console.WriteLine("Internal String: " + accessModifiers.InternalString);
            Console.WriteLine("Prorected Internal String: " + accessModifiers.ProtectedInternalString);      

        }
        /// <summary>
        /// Get Property value from derived class and sealed class.
        /// </summary>
        public void GetAccessModifiersDerivedClass()
        {

            SampleSealedClass sealedClass = new SampleSealedClass();

            sealedClass.SealedClassProperty = "This is from Sealed Class";

            this.PublicString = "Public String";

            this.ProtectedInternalString = "Protected Internal String";

            this.InternalString = "Internal String accessed by derived class";

            this.ProtectedString = "Protected String accessed by derived class";

            Console.WriteLine("Property from Sealed Class: " + sealedClass.SealedClassProperty);

            Console.WriteLine("Public String: " + this.PublicString);        
            Console.WriteLine("Internal String: " + this.InternalString);
            Console.WriteLine("Prorected String: " + this.ProtectedString);
            Console.WriteLine("Prorected Internal String: " + this.ProtectedInternalString);

        }
    }
}
